/*#define BLOCKSIZE 32
void do_block(in n, int si, int sj, int sk, double *A, double *B, double *C) { 	
	for (int i= si; i < si+BLOCKSIZE; ++i) 
		for (int j= sj; j < sj+BLOCKSIZE; ++j) { 
			double cij = C[i+j*n]; 
			for( int k = sk; k < sk+BLOCKSIZE; k++) 
				cij += A[i+k*n] * B[k+j*n]; 
			C[i+j*n] = cij;
		}
} 

void dgemm(int n, double* A, double *B, double *C) { 
	for(int sj = 0; sj < n; sj += BLOCKSIZE) 
		for(int si=0; si < n; si += BLOCKSIZE ) 
			for (int sk=0; sk < n; sk += BLOCKSIZE) 
				do_block(n, si, sj, sk, A, B, C); 

} 
*/

BLOCKSIZE .equ 32 
