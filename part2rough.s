//Original code taken from Figure 2 from Lab 11 Handout and the matrix multiply code from pages 250-253 in Chapter 3 of COD4e
//CPEN Lab 11 part 2
//Author: Joshua Marangoni 

	//////////////////////////////////////////
	//THINGS THAT DID NOT WORK 
	//MACRO 
	//N $parameter="128"
	//MEND 
	//Nsquare: .data 16384
	//REMINDER
	//16^2 = 256
	//128^2 = 16384
	/////////////////////////////////////////

	N: .data 2
	A:	.fill 4, 8, 0xDEADBEEF     //square matrix of size N^2
	B:	.fill 4, 8, 0xDEADBEEF
	C:	.fill 4, 8, 0xDEADBEEF
	
	//rename the registers to make it easier to read and write the matrix multiply code
	x 			.req 	R0    // going to get address of C 
	y 			.req 	R1    // going to get address of A
	z 			.req 	R2    // going to get address of B
	i 			.req 	R3    // local variable i
	j 			.req 	R4    // local variable j
	k 			.req 	R5    // local variable k
	xijAddr 	.req 	R6    // address of x[i][j]
	tempAddr 	.req 	R12   // address of y[i][j] or z[i][j]


	.text
	.global _start
_start:
	//BL CONFIG_VIRTUAL_MEMORY
	////////////////////////////////////////////////////////////////////////
	//Step 1-3: configure PMN0 to count cycles
	MOV R0, #0 // Write 0 into R0 then PMSELR
	MCR p15, 0, R0, c9, c12, 5 // Write 0 into PMSELR selects PMN0
	MOV R1, #0x11 // Event 0x11 is CPU cycles
	MCR p15, 0, R1, c9, c13, 1 // Write 0x11 into PMXEVTYPER (PMN0 measure CPU cycles)
	
	//Step 1-3: configure PMN1 to count load instructions
	MOV R2, #1 // Write 1 into R2 then PMSELR
	MCR p15, 0, R2, c9, c12, 5 // Write 1 into PMSELR selects PMN1
	MOV R3, #0x6 // Event 0x6 Number of load instructions executed 
	MCR p15, 0, R3, c9, c13, 1 // Write 0x6 into PMXEVTYPER (PMN1 measure # load instrs.)
	
	//Step 1-3: configure PMN2 to count Level 1 data cache misses 
	MOV R4, #2 // Write 2 into R3 then PMSELR
	MCR p15, 0, R4, c9, c12, 5 // Write 2 into PMSELR selects PMN2
	MOV R5, #0x3 // Event 0x3 is Level 1 data cache misses
	MCR p15, 0, R5, c9, c13, 1 // Write 0x3 into PMXEVTYPER (PMN2 measure Level 1 data cache misses)
	
	//////////////////////////////////////////////////////////////////////
	//Step 4: enable PMN0, PMN1, PMN2
	mov R0, #0b111 // enable bits 0, 1, and 2. (One-hot)
	MCR p15, 0, R0, c9, c12, 1 // Setting bit 0 of PMCNTENSET enables PMN0
	
	/////////////////////////////////////////////////////////////////////
	// Step 5: clear all counters and start counters 
	mov r0, #3 // bits 0 (start counters) and 1 (reset counters)
	MCR p15, 0, r0, c9, c12, 0 // Setting PMCR to 3
	
	/////////////////////////////////////////////////////////////////////
	//Step 6: code we wish to profile using hardware counters - matrix multiply 
	
	LDR R0, =A       //load base address of Matrix A 
	LDR R1, =B       //load base address of Matrix B 
	LDR R2, =C       //load base address of Matrix C
	LDR R11, =N     
	
		
	//The body of the procedure starts with initializing the three for loop variables
	MOV i, #0 							// i = 0; initialize 1st for loop
L1: MOV j, #0 							// j = 0; restart 2nd for loop
L2: MOV k, #0 							// k = 0; restart 3rd for loop

	MUL i, i, R11                       //i = i * size(row)
	ADD xijAddr, j, i		 			// xijAddr = i*size(row) + j
	ADD xijAddr, x, xijAddr, LSL #3 	// xijAddr = byte address of x[i][j]
	.word  0xED164B00   				//FLDD s4, [xijAddr,#0] 	// s4 = 8 bytes of x[i][j]
	
L3: MUL k, k, R11                       // k = k*size(row) 
	ADD tempAddr, j , k 		 		// tempAddr = k * size(row) + j
	ADD tempAddr, z, tempAddr, LSL #3	// tempAddr=byte address of z[k][j]
	.word  0xED1CFB00   			    //FLDD s15, [tempAddr,#0] 		// s16 = 8 bytes of z[k][j]
	
	MUL i, i, R11                       // i = i*size(row) 
	ADD tempAddr, k, i	 	 			// tempAddr = i * size(row) + k
	ADD tempAddr, y, tempAddr, LSL #3 	// tempAddr=byte address of y[i][k]
	.word 0xED1C8B00    				//FLDD s8, [tempAddr,#0] 	// s8 = 8 bytes of y[i][k]
	
	.word 0xEE2FFB08   					//FMULD s15, s8, s15        // s16 = y[i][k] * z[k][j]
	.word 0xEE344B0F   					//FADDD s4, s4, s15			// s4 = x[i][j]+ y[i][k] * z[k][j]
	
	ADD k, k, #1 						// k = k + 1
	CMP k, #N
	BLT L3 								// if (k < N) go to L3
	.word 0xED064B00   					//FSTD s4, [xijAddr,#0] 		// x[i][j] = s4
	
	ADD j, j, #1 						// j = j + 1
	CMP j, #N
	BLT L2 								// if (j < N) go to L2
	ADD i, i, #1 						// i = i + 1
	CMP i, #N
	BLT L1 								// if (i < N) go to L1
	
	/* 
	ldr R10, =0x568
	.word 0xED0AFB00
	ldr R10, =0x570
	.word 0xED0A4B00
	ldr R10, =0x560
	.word 0xED0A8B00
	ldr R10, =0x558
	.word 0xED0AFB00
	ldr R10, =0x550
	.word 0xED0A4B00
	*/
	
	/////////////////////////////////////////////////////////////////////
	//Step 7: stop counters 
	mov r0, #0
	MCR p15, 0, r0, c9, c12, 0 // Write 0 to PMCR to stop counters
	
	/////////////////////////////////////////////////////////////////////
	//Step 8-10: Select PMN0 and read out result into R3 
	mov r0, #0 // PMN0
	MCR p15, 0, R0, c9, c12, 5 // Write 0 to PMSELR
	MRC p15, 0, R3, c9, c13, 2 // Read PMXEVCNTR into R3
	
	// Step 8-10: Select PMN1 and read out result into R4
	mov r1, #1 // PMN1
	MCR p15, 0, R1, c9, c12, 5 // Write 1 to PMSELR
	MRC p15, 0, R4, c9, c13, 2 // Read PMXEVCNTR into R4
	
	// Step 8-10: Select PMN2 and read out result into R5
	mov r1, #2 // PMN2
	MCR p15, 0, R1, c9, c12, 5 // Write 2 to PMSELR
	MRC p15, 0, R5, c9, c13, 2 // Read PMXEVCNTR into R5
	//////////////////////////////////////////////////////////////////////////
	
end: b end            //wait here